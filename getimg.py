"""使用Google search，找到某則新聞的縮圖
Steps 1: 
    https://www.google.com/search?tmb=isch&q={NEWS TITLE}&hl=zh-TW&gl=TW&ceid=TW:zh-Hant
    Note: tmb=isch --> search image

Step 2:
    找到所有的 <img ...>
    找到第一個 src 是 http開頭的 URL，當做新聞縮圖 
"""

import requests
from requests_html import HTMLSession

URL="https://www.google.com/search"


def google_search_img(query):
    session = HTMLSession()
    my_parms = { 
            'tbm':'isch', 
            'q':query,
            'hl': 'zh-TW',
            'gl': 'TW',
            'ceid': 'TW:zh-Hant'}
    r = session.get(URL, params = my_parms)
    imgs = r.html.xpath('//img')

    imgurl = ""
    ## Find the first image src starts with "http"
    for img in imgs:
        if 'data-src' in img.attrs:
            src = img.attrs['data-src']
            if src.startswith('http'):
                imgurl = src
                break
    
    return imgurl

if __name__ == '__main__':
    
    newstitle = "台積電將赴日興建新晶圓廠，日經列5 大問題一次說清楚| TechNews 科技新報 - 科技新報 TechNews"
    imgurl = google_search_img(newstitle)
    print(newstitle)
    print(imgurl)

    newstitle = "台積電重返600元大關台股收復月線失土 - 工商時報"
    imgurl = google_search_img(newstitle)
    print(newstitle)
    print(imgurl)
    