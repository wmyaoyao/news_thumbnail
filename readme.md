# Get the 1st image from google search
See:
 - https://stackoverflow.com/questions/21530274/format-for-a-url-that-goes-to-google-image-search 
 - https://stackoverflow.com/questions/3018428/getting-the-first-url-of-an-image-search-result-with-google-image-api-in-php

Search URL:
- https://www.google.com/search?q={QUERY}&tbm=isch"

### packages
pip install -r requirements.txt

### Example:
```bash
$ python getimg.py
台積電將赴日興建新晶圓廠，日經列5 大問題一次說清楚| TechNews 科技新報 - 科技新報 TechNews
https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSs_dcKyfUXtuJHtbB9YKrMR3L4jsUGuUPNqg&usqp=CAU
台積電重返600元大關台股收復月線失土 - 工商時報
https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTZotZTLapk9u1KL4nQZ1PRaJ2vwm2G59XAGw&usqp=CAU
```